########################################
General Literature Reference Information
########################################
The files in this directory contain text excerpts from different literary works. Below, the works are listed next to the files that refer to them. 
The literature reference information is assigned to the corresponding text exerpt in the files more detailed (down to verse, line or section level).
Please read the article this source is linked by for more information on the operation data output. 


#######################
Files in this directory
#######################
Bernard_works.csv
Bernard_works.ods
Grc_reuse_1-60_refs_ordered.csv 
Lat_reuse_1-100_refs_ordered.csv
log_grc_refs_ordered.csv
log_lat_refs_ordered.csv


################## 
Greek file sources
##################
Grc_reuse_1-60_refs_ordered.csv and log_grc_refs_ordered.csv use following works:

Bible rsources:
---------------
Old Testament:
Alfred Rahlfs, editor. 1935. Septuaginta, id est Vetus Testamentum Graece juxta LXX interpretes. Rahlfs. 2 vol., 1950.
New Testament:
Kurt Aland and Barbara Aland, editors. 1966. The Greek New Testament. Deutsche Bibelgesellschaft-United Bible Societies, 27 edition.
(The detailed information next to the data entry consits of a link to the BibleIndex parsing tools page down to verse detail.)

Patristic resource:
-------------------
Clément d'Alexandrie, Quel riche sera sauvé ?, Quis dives salvetur, P. A. O à Sources Chrétiennes, col. 537, p. 100 ff., 2011.


##################
Latin file sources
##################
Lat_reuse_1-100_refs_ordered.csv and log_lat_refs_ordered.csv use the following works:

Bible resource:
---------------
Gribomont J. Weber R., Fischer B., editor. 1969, 1994, 2007. Biblia sacra juxta vulgatam versionem. Deutsche Bibelgesellschaft.
(The detailed information next to the data entry consits of a link to the BibleIndex parsing tools page down to verse detail.)

Patristic resources:
--------------------
Bernard de Clairvaux, Sermons sur le Cantique 1-15, Sermones super Cantica Canticorum 1-15, P. A. O à Sources Chrétiennes, col. 414, 1996.
Bernard de Clairvaux, Sermons sur le Cantique 16-32, Sermones super Cantica Canticorum 16-32 , P. A. O à Sources Chrétiennes, col. 431, 1998.
Bernard de Clairvaux, Sermons sur le Cantique 33-50, Sermones super Cantica Canticorum 33-50, , P. A. O à Sources Chrétiennes, col. 452, 2000.
Bernard de Clairvaux, Sermons sur le Cantique 51-68, Sermones super Cantica Canticorum 151-68 , P. A. O à Sources Chrétiennes, col. 472, 2003.
Bernard de Clairvaux, Sermons sur le Cantique 69-86, Sermones super Cantica Canticorum 69-86 , P. A. O à Sources Chrétiennes, col. 511, 2007.
Bernard de Clairvaux, Amour de Dieu, Liber de diligendo Deo, P. A. O à Sources Chrétiennes, col. 393, 1993.
Bernard de Clairvaux, Lettres 1-41, Epistolae 1-41, P. A. O à Sources Chrétiennes, col. 425, 1997.
Bernard de Clairvaux, Lettres 42-91, Epistolae 42-91, P. A. O à Sources Chrétiennes, col. 425, 1997.
Bernard de Clairvaux, Lettres 92-163, Epistolae 92-163, P. A. O à Sources Chrétiennes, col. 425, 1997.
Bernard de Clairvaux, Lettres 363-495, Epistolae 363-495, SBO VIII, 311-447 1957-1977.
Bernard de Clairvaux, Éloge de la nouvelle chevalerie, Liber ad milites Templi De laude novae militiae, P. A. O à Sources Chrétiennes,col. 367, 1990.
Bernard de Clairvaux, Précepte et la Dispense, Liber de praecepto et dispensatione, P. A. O à Sources Chrétiennes, col. 457, 2000.
Bernard de Clairvaux, Grâce et le Libre Arbitre, Liber de gratia et de libero arbitrio, P. A. O à Sources Chrétiennes, col. 393, 1993.
Bernard de Clairvaux, Degrés de l'humilité et de l'orgueil, Liber de gradibus humilitatis et superbia, , SBO III, 13-59, 1957-1977.
Bernard de Clairvaux, Vie de saint Malachie, Vita sancti Malachiae episcopi, P. A. O à Sources Chrétiennes, col. 367, 1990.
Bernard de Clairvaux, Sermon lors de la mort de Malachie, Sermo in transitu sancti Malachiae, P. A. O à Sources Chrétiennes, col. 367, 1990
Bernard de Clairvaux,Sentences, Sententiae, SBO VI-2, 1-256, 1957-1977.
Bernard de Clairvaux, Conversion (Aux clercs sur la conversion), Sermo de conuersione ad clericos, P. A. O à Sources Chrétiennes, col. 457, 2000.
Bernard de Clairvaux, Louange de la Vierge Mère, Homiliae super Missus est (In laudibus Virginis Matris), P. A. O à Sources Chrétiennes, col. 390, 1993.
Bernard de Clairvaux, Sermon pour la Nativité de la Bienheureuse Vierge Marie, Sermo in natiuitate Beatae Mariae Virginis,  SBO V, 275-288, 1957-1977.
Bernard de Clairvaux, Sermons pour la naissance de saint Victor, Sermones varii : In natali sancti Victoris, col. 526, 2010.

                                                         
###############################################################
Detailed structure information of automatically generated files 
###############################################################
log_grc_refs_ordered.csv and log_lat_refs_ordered.csv are structured as follows:

bible_ref_as_ID \tab bible_reference_link \tab SC_ref* \tab operations_per_word(reuse_word,bible_verse_word) \tab operations_per_word(reuse_word,bible_verse_word) \tab ...


##########################################################
Detailed structure information of manually generated files 
##########################################################
Grc_reuse_1-60_refs_ordered.csv and Lat_reuse_1-100_refs_ordered.csv are structured as follows:

bible_ref_as_ID \tab tab_separated_row_of_patristic_source_snippet(not necessarily_in_original_order)
bible_ref_as_ID \tab tab_separated_row_of_bible_verse(not necessarily_in_original_order)
                \tab tab_separated_row_of_operation(s)_per_word
bible_reference_link
SC_ref*

... 
                
With *SC_ref = 
Source_Cretiennes_page,Source_Cretiennes_line (for the Latin files)
Source_Cretiennes_section,Source_Cretiennes_subsection p.Source_Cretiennes_page (for the Greek files)

####################################
Bernard work and edition information 
####################################
We also publish an overview table (ODT-file and CSV-file) in this directory. It maps the Bernard reuse that we use to the corresponding work names and printed book names, and list the numbers of reuses per work. 